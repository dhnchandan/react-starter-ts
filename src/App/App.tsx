import React, { FC } from "react";

const App: FC = () => {
	return (
		<div className="App">
			<h1>React Starter TS</h1>
		</div>
	);
};

export default App;
